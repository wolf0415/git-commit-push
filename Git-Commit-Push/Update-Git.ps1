<#
.SYNOPSIS
Scripts for Git commits
.DESCRIPTION
Automate the process of git add and commits
.PARAMETER message
Specify commit messages
.PARAMETER file
Names of file that you want to commit 
.PARAMETER Push
Push to origin master
.PARAMETER refs 
Specify refs that you want to push to (master or main)
.Example
PS C:\> Update-Git -m "<git commit messages>" -file file1,file2
When specify multiple files, you can add comma (",") between files
.Example
PS C:\> Update-Git -m "<git commit messages>" -file file1
Git add and commit messages (single file)
.Example
PS C:\> PS C:\> Update-Git -m "<git commit messages>" -file file1 -Push 
.Example 
PS C:\> file1 | Update-Git -m "<git commit messages>" 
Name of the files support pipelining. Thus, the paramter (-file) doesn't need to specify
.Example 
PS C:\>  Update-Git -m "<git commit messages>" -file file -refs main
push to the main/master branch
#>


function Update-Git {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        [Alias('m')]
        [string]$message,
        [Switch]$Push,
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [string[]]$file,
        [ValidateSet("master", "main")]
        [string[]]$refs
    )

    Begin {}
    Process {
        git add $file.Replace(",", " ")
        git commit -m $message
        if ($Push) {
            git push origin $refs
        }            
    }
    
    End {
        Write-Output "-------------------------------------"
        git status
        Write-Output "--------------------------------------"
        git log
    }
}
