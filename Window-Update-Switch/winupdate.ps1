
function Enable-WindowsUpdate{
    [cmdletbinding()]
    Param()

    Set-Service wuauserv -StartupType Manual
    Start-Service wuauserv
    Write-Verbose "Windows Update enabled"
    
	Set-Service UsoSvc -StartupType Manual
    Start-Service UsoSvc
    Write-Verbose "Update Orchestrator Service enabled"
    
	Set-Service WaaSMedicSvc -StartupType Manual
    Start-Service WaaSMedicSvc
    Write-Verbose "Windows Update Media Service enabled"
}

function Disable-WindowsUpdate{
    [cmdletbinding()]
    Param()
    Set-Service wuauserv -StartupType Disabled
    Stop-Service wuauserv
	Write-Verbose "Windows Update disabled"

    Set-Service UsoSvc -StartupType Disabled
    Stop-Service UsoSvc
	Write-Verbose "Update Orchestrator Service disabled"

    Set-Service WaaSMedicSvc -StartupType Disabled
    Stop-Service WaaSMedicSvc
	Write-Verbose "Windows Update Media Service enabled"

}