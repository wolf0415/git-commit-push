[Workstation]
## Add windows server to trusted host 
Start-Service WinRM
Get-Item WSMan:\localhost\Client\TrustedHosts # show the trusted host 
Set-Item WSMan:\localhost\Client\TrustedHosts -Value 192.168.56.106
Get-ChildItem WSMan:\localhost\Client\TrustedHosts # check if machine's ip are added into Trusted IP list 
New-PSSession -ComputerName 192.168.56.106 -Credential (Get-Credential) # Enter the domain controller username and password

[DC]
## AT HERE: [192.168.56.106]: PS C:\Users\Administrator\Documents>
sconfig # to change the configuration of the server 
# select 2 for changing the name of the server 
# select 8 for changing the network adapter settings (Recommended: change the dc to static ip,dns server ...) 

## Install AD command tools in DC 
Install-WindowsFeature AD-Domain-Services -InstallManagementTools


## Configure AD Windows Server 2022 Core
Import-Module ADDSDeployment 
Install-ADDSForest
cmdlet Install-ADDSForest at command pipeline position 1
# Supply values for the following parameters:
# DomainName: xyz.com
# WARNING: A script or application on the remote computer 192.168.56.106 is sending a prompt request. When you are prompted, enter sensitive information, such as credentials
# or passwords, only if you trust the remote computer and the application or script that is requesting the data.
# SafeModeAdministratorPassword: ************
# WARNING: A script or application on the remote computer 192.168.56.106 is sending a prompt request. When you are prompted, enter sensitive information, such as credentials
# or passwords, only if you trust the remote computer and the application or script that is requesting the data.
# Confirm SafeModeAdministratorPassword: ************

# The target server will be configured as a domain controller and restarted when this operation is complete.
# Do you want to continue with this operation?
# [Y] Yes  [A] Yes to All  [N] No  [L] No to All  [?] Help (default is "Y"): Y
 
## Set the DNS server ip addr 
Get-DnsClientServerAddress # see what is our current dns settings; see the interface index
Set-DnsClientServerAddress -InterfaceIndex 3 -ServerAddresses (Get-NetIPAddress -AddressFamily IPv4 | Where-Object { $_.IPAddress -ne "127.0.0.1" }).IPAddress

[Workstation]
## Access work or school 
# Go to one of the workstation and search "Access work or school"
Add-computer -DomainName xyz.com -Credential xyz\Administrator -Force -Restart # enter the password of the dc 
